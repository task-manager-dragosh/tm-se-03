package ru.dragosh.tm.database;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataBase {
    public static final String BORDER = "----------------------------------------------------------------------------------------------------------------------------------";
    public static final String HELP = "help";
    public static final String DESCRIPTION_HELP = " (выводит на дисплей список доступных команд)";

    public static final String START_DISPLAY = "\n" +
            "PROJECT BASE\n" +
            BORDER + "\n" +
            "Введите \"help\"" + DESCRIPTION_HELP + " или нужную команду\n" +
            BORDER;

    public static final String START_DISPLAY_AGAIN = "\n" +
            "ATTENTION!\n" +
            BORDER + "\n" +
            "Введите \"help\"" +
            DESCRIPTION_HELP +
            " или нужную команду\n" +
            BORDER;

    public static final String SHOW_ALL = "show all";
    public static final String SHOW_TASKS = "show tasks";
    public static final String ADD_PROJECT = "add project";
    public static final String ADD_TASK = "add task";
    public static final String REMOVE_PROJECT = "remove project";
    public static final String REMOVE_TASK = "remove task";
    public static final String EXIT = "exit";
    public static boolean PROGRAM_EXECUTION = false;

    public static final String DESCRIPTION_SHOW_ALL = " (выводит на дисплей список проектов и задач из Project base)";
    public static final String DESCRIPTION_SHOW_TASKS = " (выводит на дисплей список задач проекта из Project base)";
    public static final String DESCRIPTION_ADD_PROJECT = " (добавляет в Project base новый проект)";
    public static final String DESCRIPTION_ADD_TASK = " (добавляет в проект новую задачу)";
    public static final String DESCRIPTION_REMOVE_PROJECT = " (удаляет из Project base проект)";
    public static final String DESCRIPTION_REMOVE_TASK = " (удаляет из проекта задачу)";
    public static final String DESCRIPTION_EXIT = " (после завершения ввода названия/названий введите \"Exit\" + Enter)";

    
     public static List<String> commandsList = new ArrayList<String>(){{
        add(SHOW_ALL + DESCRIPTION_SHOW_ALL);
        add(SHOW_TASKS + DESCRIPTION_SHOW_TASKS);
        add(ADD_PROJECT + DESCRIPTION_ADD_PROJECT + DESCRIPTION_EXIT);
        add(ADD_TASK + DESCRIPTION_ADD_TASK+ DESCRIPTION_EXIT);
        add(REMOVE_PROJECT + DESCRIPTION_REMOVE_PROJECT);
        add(REMOVE_TASK + DESCRIPTION_REMOVE_TASK);
        add(EXIT + DESCRIPTION_EXIT);
    }};

    public static Map<String, Project> projects = new LinkedHashMap<>();

    static {
        try {
            Project project1 = new Project("project1", "test project", "31.10.2019", "31.11.2019");
            Project project2 = new Project("project2", "test project 2", "30.10.2019", "30.11.2019");
            Project project3 = new Project("project3", "test project 2", "30.10.2019", "30.11.2019");

            projects.put(project1.getIDProject(), project1);
            projects.put(project2.getIDProject(), project2);
            projects.put(project3.getIDProject(), project3);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }

    public static Map<String, Task> tasks = new LinkedHashMap<>();
}
