package ru.dragosh.tm;
import ru.dragosh.tm.business_logic.Bootstrap;

import static ru.dragosh.tm.database.DataBase.*;

public class App
{
    public static void main( String[] args ) {
        System.out.println(START_DISPLAY);
        PROGRAM_EXECUTION = true;
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}