package ru.dragosh.tm.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Project {
    private String IDProject = "";
    private String nameProject = "";
    private String descriptionProject = "";
    private Date dateProjectStart;
    private Date dateProjectFinish;

    public Project() {
        generateUUID();
    }

    public Project(String nameProject, String descriptionProject, String dateProjectStart, String dateProjectFinish) throws ParseException{
        this();
        this.nameProject = nameProject;
        this.descriptionProject = descriptionProject;
        this.setDateProjectStart(dateProjectStart);
        this.setDateProjectFinish(dateProjectFinish);
    }

    public String getIDProject() {
        return IDProject;
    }

    private void generateUUID() {
        this.IDProject = UUID.randomUUID().toString();
    }

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }

    public String getDescriptionProject() {
        return descriptionProject;
    }

    public void setDescriptionProject(String descriptionProject) {
        this.descriptionProject = descriptionProject;
    }

    public Date getDateProjectStart() {
        return dateProjectStart;
    }

    public void setDateProjectStart(String dateProjectStart) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateProjectStart = dt.parse(dateProjectStart);
    }

    public Date getDateProjectFinish() {
        return dateProjectFinish;
    }

    public void setDateProjectFinish(String dateProjectFinish) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateProjectFinish = dt.parse(dateProjectFinish);
    }

    @Override
    public String toString() {
        return "UUID проекта: " + this.IDProject + ";\n" +
                "Название проекта: " + this.nameProject + ";\n" +
                "Описание проекта: " + this.descriptionProject + ";\n" +
                "Дата начала проекта: " + this.dateProjectStart + ";\n" +
                "Дата окончания проекта: " + this.dateProjectFinish + ";";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;

        if (IDProject == null || nameProject == null)
            return false;

        return nameProject.equals(project.nameProject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(IDProject, nameProject, descriptionProject, dateProjectStart, dateProjectFinish);
    }
}
