package ru.dragosh.tm.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Task {
    private String IDTask;
    private String nameTask;
    private String descriptionTask;
    private Date dateTaskStart;
    private Date dateTaskFinish;
    private String IDProjectForTask;

    public Task() {
        generateUUID();
    }

    public Task(String nameTask, String descriptionTask, String dateTaskStart, String dateTaskFinish, String IDProjectForTask) throws ParseException {
        this();
        this.nameTask = nameTask;
        this.descriptionTask = descriptionTask;
        this.setDateTaskStart(dateTaskStart);
        this.setDateTaskFinish(dateTaskFinish);
        this.IDProjectForTask = IDProjectForTask;
    }

    public String getIDTask() {
        return IDTask;
    }

    private void generateUUID() {
        this.IDTask = UUID.randomUUID().toString();
    }

    public String getNameTask() {
        return nameTask;
    }

    public void setNameTask(String nameTask) {
        this.nameTask = nameTask;
    }

    public String getDescriptionTask() {
        return descriptionTask;
    }

    public void setDescriptionTask(String descriptionTask) {
        this.descriptionTask = descriptionTask;
    }

    public Date getDateTaskStart() {
        return dateTaskStart;
    }

    public void setDateTaskStart(String dateTaskStart) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateTaskStart = dt.parse(dateTaskStart);
    }

    public Date getDateTaskFinish() {
        return dateTaskFinish;
    }

    public void setDateTaskFinish(String dateTaskFinish) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateTaskFinish = dt.parse(dateTaskFinish);
    }

    public String getIDProjectForTask() {
        return IDProjectForTask;
    }

    public void setIDProjectForTask(String IDProjectForTask) {
        this.IDProjectForTask = IDProjectForTask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;

        if (nameTask == null)
            return false;

        return nameTask.equals(task.nameTask);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameTask);
    }

    @Override
    public String toString() {
        return "UUID задачи: " + this.IDTask+ ";\n" +
                "Название задачи: " + this.nameTask + ";\n" +
                "Описание задачи: " + this.descriptionTask + ";\n" +
                "Дата начала выполнения задачи: " + this.dateTaskStart + ";\n" +
                "Дата окончания выполнения задачи: " + this.dateTaskFinish + ";";
    }
}
