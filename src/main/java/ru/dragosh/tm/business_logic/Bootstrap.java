package ru.dragosh.tm.business_logic;

import ru.dragosh.tm.database.DataBase;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;

import java.text.ParseException;
import java.util.Map;
import java.util.Scanner;

import static ru.dragosh.tm.database.DataBase.BORDER;
import static ru.dragosh.tm.database.DataBase.EXIT;

public class Bootstrap {

    public void start() {
        Controller controller = new Controller();
        while(DataBase.PROGRAM_EXECUTION) {
            String command = scanCommand();
            controller.choiceCommand(command);
        }
        System.err.println("ВЫХОД ИЗ ПРОГРАММЫ!");
    }

    public String scanCommand() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите команду: ");
        String string = scanner.nextLine();
        return string.toLowerCase(); // Все введенные знаки передаются строчными.
    }

    private String scanWord() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public void helps() {
        System.out.println("Введите нужную команду:");
        System.out.println(BORDER);
        for (String s : DataBase.commandsList) {
            System.out.println(s);
        }
        System.out.println(BORDER);
    }

    public void createProject() {
        while (true) {
            System.out.print("Введите название проекта: ");
            String nameProject = scanWord();
            if (EXIT.equals(nameProject.toLowerCase()))
                break;
            System.out.print("Введите описание проекта: ");
            String descriptionProject = scanWord();
            System.out.print("Введите дату начала проекта (dd.mm.yyyy): ");
            String dateProjectStart = scanWord();
            System.out.print("Введите дату окончания проекта (dd.mm.yyyy): ");
            String dateProjectFinish = scanWord();
            if(EXIT.equals(descriptionProject)
                    || EXIT.equals(dateProjectStart) || EXIT.equals(dateProjectFinish)) {
                break;
            }

            Project project = null;
            try {
                project = new Project(nameProject, descriptionProject, dateProjectStart, dateProjectFinish);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (DataBase.projects.containsValue(project)) {
                System.err.println("ПРОЕКТ С ТАКИМ НАЗВАНИЕМ УЖЕ СУЩЕСТВУЕТ");
                return;
            }

            addProject(project);
            showProjects();
        }
    }

    private void addProject(Project project) {
        DataBase.projects.put(project.getIDProject(), project);
    }

    public void removeProject() {
        while (true) {
            System.out.print("Введите название проекта: ");
            String nameProject = scanWord();
            if (EXIT.equals(nameProject))
                break;
            String uuidProject = "";
            for (Map.Entry<String, Project> project: DataBase.projects.entrySet()) {
                if (project.getValue().getNameProject().equals(nameProject)) {
                    uuidProject = project.getKey();
                    break;
                }
            }

            for (Map.Entry<String, Task> task: DataBase.tasks.entrySet()) {
                String uuidTask = "";
                if (task.getValue().getIDProjectForTask().equals(uuidProject)) {
                    uuidTask = task.getKey();
                    DataBase.tasks.remove(uuidTask);
                }
            }

            DataBase.projects.remove(uuidProject);
        }
    }

    public void createTask() {
        while (true) {
            String uuidProject = "";
            System.out.print("Введите название проекта: ");
            String nameProject = scanWord();

            if (EXIT.equals(nameProject.toLowerCase()))
                break;

            for (Map.Entry<String, Project> project: DataBase.projects.entrySet()) {
                if (project.getValue().getNameProject().equals(nameProject)) {
                    uuidProject = project.getKey();
                }
            }

            if (uuidProject.isEmpty()) {
                System.err.println("ПРОЕКТА С ТАКИМ ИМЕНЕМ НЕ СУЩЕСТВУЕТ");
                break;
            }

            System.out.print("Введите название задачи: ");
            String nameTask = scanWord();
            System.out.print("Введите описание задачи: ");
            String descriptionTask = scanWord();
            System.out.print("Введите дату начала выполнения задачи (dd.mm.yyyy): ");
            String dateTaskStart = scanWord();
            System.out.print("Введите дату окончания выполнения задачи (dd.mm.yyyy): ");
            String dateTaskFinish = scanWord();

            if(EXIT.equals(nameTask) || EXIT.equals(descriptionTask)
                    || EXIT.equals(dateTaskStart) || EXIT.equals(dateTaskFinish)) {
                break;
            }

            Task task = null;
            try {
                task = new Task(nameTask, descriptionTask, dateTaskStart, dateTaskFinish, uuidProject);
                if (DataBase.tasks.containsValue(task)) {
                    System.err.println("ЗАДАЧА В ДАННОМ ПРОЕКТЕ С ТАКИМ ИМЕНЕМ УЖЕ СУЩЕСТВУЕТ");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

            addTask(task);
            showTasks(uuidProject);
        }
    }

    private void addTask(Task task) {
        DataBase.tasks.put(task.getIDTask(), task);
    }

    public void removeTask() {
        while (true) {
            String uuidProject = "";
            System.out.print("Введите название проекта: ");
            String nameProject = scanWord();

            if (EXIT.equals(nameProject.toLowerCase()))
                break;

            for (Map.Entry<String, Project> project: DataBase.projects.entrySet()) {
                if (project.getValue().getNameProject().equals(nameProject)) {
                    uuidProject = project.getKey();
                }
            }

            if (uuidProject.isEmpty()) {
                System.err.println("ПРОЕКТА С ТАКИМ ИМЕНЕМ НЕ СУЩЕСТВУЕТ");
                break;
            }

            System.out.print("Введите название задачи: ");
            String nameTask = scanWord();
            if (EXIT.equals(nameTask))
                break;

            for (Map.Entry<String, Task> task: DataBase.tasks.entrySet()) {
                String uuidTask = "";
                Task t = task.getValue();
                if (t.getNameTask().equals(nameTask) && t.getIDProjectForTask().equals(uuidProject)) {
                    uuidTask = task.getKey();
                    DataBase.tasks.remove(uuidTask);
                }
            }
        }
    }

    public void showProjects() {
        System.out.println(BORDER);
        for (Map.Entry<String, Project> project: DataBase.projects.entrySet()) {
            System.out.println(project.getValue());
        }
        System.out.println(BORDER);
    }

    public void showTasks(String uuidProject) {
        System.out.println(BORDER);
        System.out.println("Номер проекта: " + uuidProject + ";");
        System.out.println("Название проекта: " + DataBase.projects.get(uuidProject).getNameProject() + ";\n");
        for (Map.Entry<String, Task> task: DataBase.tasks.entrySet()) {
            Task t = task.getValue(); //!!!!
            if (t.getIDProjectForTask().equals(uuidProject)) {
                System.out.println(t);
            }
        }
        System.out.println(BORDER);
    }

    public void showAll() {
        for (Map.Entry<String, Project> p: DataBase.projects.entrySet()) {
            String uuidProject = p.getKey();
            Project project = p.getValue();
            System.out.println(project);
            System.out.println(BORDER);
            for (Map.Entry<String, Task> t: DataBase.tasks.entrySet()) {
                Task task = t.getValue();
                if (task.getIDProjectForTask().equals(uuidProject)) {
                    System.out.println(task);
                }
            }
            System.out.println(BORDER);
        }
    }

    public void showTasks() {
        System.out.print("Введите название проекта: ");
        String nameProject = scanWord();
        Project pr = new Project();
        pr.setNameProject(nameProject);
        if (DataBase.projects.containsValue(pr))
            for (Map.Entry<String, Project> p: DataBase.projects.entrySet()) {
                String uuidProject = p.getKey();
                Project project = p.getValue();
                System.out.println(project);
                System.out.println(BORDER);
                if (project.getNameProject().equals(nameProject)) {
                    for (Map.Entry<String, Task> t : DataBase.tasks.entrySet()) {
                        Task task = t.getValue();
                        if (task.getIDProjectForTask().equals(uuidProject)) {
                            System.out.println(task);
                        }
                    }
                    System.out.println(BORDER);
                    break;
                }
                System.out.println(BORDER);
            }
    }
}