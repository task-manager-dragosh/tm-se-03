package ru.dragosh.tm.business_logic;


import ru.dragosh.tm.database.DataBase;

import static ru.dragosh.tm.database.DataBase.*;

public class Controller {

    Bootstrap bootstrap = new Bootstrap();
    DataBase dataBase = new DataBase();

    public void choiceCommand(String string) {
        switch(string) {
            case HELP: {
                bootstrap.helps();
                break;
            }
            case SHOW_ALL: {
                bootstrap.showAll();
                break;
            }

            case SHOW_TASKS: {
                bootstrap.showTasks();
                break;
            }

            case ADD_PROJECT: {
                bootstrap.createProject();
                break;
            }

            case ADD_TASK: {
                bootstrap.createTask();
                break;
            }

            case REMOVE_PROJECT: {
                bootstrap.removeProject();
                break;
            }

            case REMOVE_TASK: {
                bootstrap.removeTask();
                break;
            }

            case EXIT: {
                PROGRAM_EXECUTION = false;
                break;
            }

            default:
                System.err.println(START_DISPLAY_AGAIN);
        }
    }
}
