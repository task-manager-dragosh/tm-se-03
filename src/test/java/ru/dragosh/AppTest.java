package ru.dragosh;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import ru.dragosh.tm.business_logic.Bootstrap;
import ru.dragosh.tm.database.DataBase;
import ru.dragosh.tm.entity.Project;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testCreateProject() throws ParseException {
        Bootstrap bootstrap = new Bootstrap();
        Map<String, Project> projects = new LinkedHashMap<>();
        Project project1 = new Project("project1", "test project", "31.10.2019", "31.11.2019");
        Project project2 = new Project("project2", "test project 2", "30.10.2019", "30.11.2019");
        Project project3 = new Project("project3", "test project 2", "30.10.2019", "30.11.2019");
        projects.put(project1.getIDProject(), project1);
        projects.put(project2.getIDProject(), project2);
        projects.put(project3.getIDProject(), project3);

        bootstrap.createProject();
        bootstrap.createProject();
        bootstrap.createProject();

        Assert.assertEquals("Не совпадают", projects, DataBase.projects);
    }

}
